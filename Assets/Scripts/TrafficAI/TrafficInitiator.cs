﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficInitiator : MonoBehaviour {
    public List<Intersection> intersections = new List<Intersection>();
    public int vehicleAmount = 18;
   
    public GameObject vehiclePrefab; // ** Make list of vehiclePrefabs and instantiate randomly **

    // Use this for initialization
    void Start ()
    {
        intersections.AddRange(FindObjectsOfType<Intersection>());
        SpawnVehicleAmount();
	}

    void SpawnVehicleAmount()
    {
        for (int i = 0; i < vehicleAmount; i++)
        {
            int j = Random.Range(0, intersections.Count);
            GameObject car = Instantiate(vehiclePrefab, intersections[j].transform.position, Quaternion.identity);
            Vehicle veh;
            if (car.GetComponent<Vehicle>() == null)
            {
                veh = car.AddComponent<Vehicle>();
            }
            else
            {
                veh = car.GetComponent<Vehicle>();
            }
            veh.origSpeed = Random.Range(2.5f, 5);
            veh.nextInter = intersections[j].connectingIntersections[Random.Range(0, intersections[j].connectingIntersections.Count)];
        }
    }
}
