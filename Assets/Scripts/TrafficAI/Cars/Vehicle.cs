﻿using UnityEngine;
using UniRx.Triggers;
using UniRx;
using System;

public class Vehicle : MonoBehaviour//, IVehicle
{
    public float origSpeed;
    public float speed;
    public Intersection nextInter;
    public Intersection prevInter;
    public Intersection cachedInter;
    //[SerializeField]
    Transform veh;
    public Renderer[] carRends;
    ObservableTriggerTrigger ott;
    void Awake()
    {
        veh = GetComponentInChildren<Transform>();
        //Adding or Getting OTT component
        ott = veh.GetComponent<ObservableTriggerTrigger>();
        if (ott == null)
        {
            ott = veh.gameObject.AddComponent<ObservableTriggerTrigger>();
        }
        Color carCol = UnityEngine.Random.ColorHSV();
        foreach(Renderer rend in carRends)
        {
            rend.material.color = carCol;
        }
    }

    void Start()
    {
        speed = origSpeed;

        ott.OnTriggerEnterAsObservable().Where(c => c.GetComponent<Intersection>() != null).Subscribe(_ => Observable.Timer(TimeSpan.FromSeconds(0.2)).Subscribe(u => GoToNextDestination()));

        ott.OnTriggerExitAsObservable().Where(c => c.GetComponent<Intersection>() != null).Subscribe(_ => StoreNextDestination(nextInter));
    }

    void StoreNextDestination(Intersection inter)
    {
        Debug.Log("Storing Destination");
        Intersection tranInt = inter.connectingIntersections[UnityEngine.Random.Range(0, inter.connectingIntersections.Count)];
        if (tranInt == prevInter)
        {
            StoreNextDestination(nextInter);
        }
        else
        {
            cachedInter = tranInt;
        }
    }

    void GoToNextDestination()
    {
        prevInter = nextInter;
        Debug.Log("Going to next Destination");
        originalAngle = transform.rotation;
        nextInter = cachedInter;
    }

    void Update()
    {
        transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        veh.transform.position = new Vector3(veh.position.x, 0, veh.position.z);
        if (nextInter != null)
        {
            LookAtInter(nextInter);
        }
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    //public void NextDestination(Intersection inter)
    //{
    //    originalAngle = transform.rotation;
    //    Intersection tranInt = inter.connectingIntersections[Random.Range(0, inter.connectingIntersections.Count)];
    //    if(tranInt == prevInter)
    //    {
    //        NextDestination(inter);
    //        return;
    //    }
    //    else
    //    {
    //        nextInter = tranInt;
    //        prevInter = inter;
    //    }
    //}

    Quaternion originalAngle;

    void LookAtInter(Intersection inter)
    {
        Vector3 dir = inter.transform.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(dir,Vector3.up);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, 0.1f);

        Vector3 current = transform.eulerAngles;
        Vector3 target = rot.eulerAngles;

        float fraction = target.y - current.y;

        if (fraction > 0.5f)
        {
            speed = 0.5f;
            //set movespeed lower
        }
        else
        {
            speed = origSpeed;
            //set movespeed higher
        }
    }
}
