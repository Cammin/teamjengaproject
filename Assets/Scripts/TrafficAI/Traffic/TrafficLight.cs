﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TrafficLight : MonoBehaviour
{
    public enum LightMode { Red, Yellow, Green }
    LightMode trafficMode;

    public float lightTimer;
    public float redTime;
    public float yellowTime;
    public float greenTime;

    public bool isRed;
    public bool isYellow;
    public bool isGreen;

    Collider col;
    //Vector3 prev; //uncomment this once ready to work on some more.
    GameObject veh;
    NavMeshAgent ag;

    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<Collider>();
        trafficMode = LightMode.Red;
    }

    // Update is called once per frame
    void Update()
    {
        switch (trafficMode)
        {
            case LightMode.Red:
                RedUpdate();
                RedEffect();
                break;
            case LightMode.Yellow:
                YellowUpdate();
                YellowEffect();
                break;
            case LightMode.Green:
                GreenUpdate();
                GreenEffect();
                break;
        }
    }

    public void RedUpdate()
    {
        Debug.Log("RedLight");
        lightTimer += Time.deltaTime;
        if (lightTimer >= redTime)
        {
            lightTimer = 0;
            trafficMode = LightMode.Green;
        }
    }

    public void YellowUpdate()
    {
        Debug.Log("YellowLight");
        lightTimer += Time.deltaTime;
        if (lightTimer >= yellowTime)
        {
            lightTimer = 0;
            trafficMode = LightMode.Red;
        }
    }

    public void GreenUpdate()
    {
        Debug.Log("GreenLight");
        
        lightTimer += Time.deltaTime;
        if (lightTimer >= greenTime)
        {
            lightTimer = 0;
            trafficMode = LightMode.Yellow;
        }
    }

    void RedEffect()
    {
        col.enabled = true;
        isRed = true;
        isYellow = false;
    }

    void YellowEffect()
    {
        col.enabled = false;
        isYellow = true;
        isGreen = false;
    }
    
    void GreenEffect()
    {
        col.enabled = false;
        isGreen = true;
        isRed = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Vehicle") 
        {
            veh = other.gameObject;
            ag = veh.GetComponent<NavMeshAgent>();
            if(isRed)
            {
                //prev = ag.destination; //hey there damir, i commented this out because it was showing up as a warning in the inspector. you can uncomment it once you are ready to work on it more.
                ag.isStopped = true;
            }
            //if (isGreen)
            //{
            //    ag.isStopped = false;
            //    Debug.Log("here");
            //}
        }
    }
}
