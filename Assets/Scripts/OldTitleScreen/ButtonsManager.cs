﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonsManager : MonoBehaviour
{
    public Transform mainPosition;
    public Transform optionsPosition;
    public Transform creditsPosition;
    public Transform helpPosition;
    

    public FollowCam mainCamera;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void GoToMain()
    {
        mainCamera.followTarget = mainPosition;
    }

    public void GoToOptions()
    {
        mainCamera.followTarget = optionsPosition;
    }

    public void GoToHelp()
    {
        mainCamera.followTarget = helpPosition;
    }

    public void GoToCredits()
    {
        mainCamera.followTarget = creditsPosition;
    }

    public void PlayGame()
    {
        SceneManagement.instance.ChangeScene(SceneManagement.instance.garageSceneName);
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("If this were a real build, the application would close.");
    }


}
