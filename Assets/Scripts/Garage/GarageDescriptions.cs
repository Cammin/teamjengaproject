﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarageDescriptions : Singleton<GarageDescriptions>
{
    [Header("Part Descriptions")]
    [TextArea(0, 20)] public string bodyDescription;
    [TextArea(0, 20)] public string engineDescription;
    [TextArea(0, 20)] public string brakesDescription;
    [TextArea(0, 20)] public string tiresDescription;

    [Header("Quality Descriptions")]
    [TextArea(0, 20)] public string rustyDescription;
    [TextArea(0, 20)] public string standardDescription;
    [TextArea(0, 20)] public string heavyDescription;
    [TextArea(0, 20)] public string sportyDescription;
    [TextArea(0, 20)] public string royalDescription;
}

