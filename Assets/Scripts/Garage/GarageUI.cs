﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GarageUI : MonoBehaviour
{
    [Header ("General Menu")]
    [SerializeField]  GameObject generalMenu;
    [SerializeField]  Button mainMenuBtn;
    [SerializeField]  Button goToWorkBtn;
                      public Button returnBtn;

    [Header("Scroll Menu")]
    [SerializeField]  GameObject upgradePanel;

    [Header("Money Texts")]
    [SerializeField] Text moneyText;

    int qualTracker;
    bool inFeature;
    UpgradesUI UpUI;


    #region OnStart
    void Start ()
    {
        UpUI = FindObjectOfType<UpgradesUI>();

        UpdateMoneyUI();
        ShowGeneralMenu(true);
        ShowPurchasingMenu(false);
        SetupDelegatesOnStart();
	}
	
    void SetupDelegatesOnStart()
    {
            // Main Menu Buttons
        mainMenuBtn.onClick.AddListener(OnMainMenu);
        goToWorkBtn.onClick.AddListener(OnGoToWork);
            // Upgrade Menu Buttons
        returnBtn.onClick.AddListener(OnReturn);
    }
    #endregion  

    #region ButtonCommands
    // Main Menu Buttons
    void OnMainMenu() 
    {
        Debug.Log("MainMenu Entrance");
        SceneManagement.instance.ChangeScene(SceneManagement.instance.titleScreenSceneName);
    }
    void OnGoToWork() 
    {
        Debug.Log("GameScene Entrance");
        SceneManagement.instance.ChangeScene(SceneManagement.instance.gameplaySceneName);
        
    }
    void OnUpgrade() 
    {
        ShowGeneralMenu(false);
    }

       // Upgrade Menu Buttons
    void OnReturn() 
    {
        ShowPurchasingMenu(false);
        if (inFeature)
        {
            UpUI.basePanelHolder.SetActive(false);
            inFeature = false;
            GarageManager.instance.DefaultView();
        }
        else
        {
            ShowGeneralMenu(true);
        }

        //rest was just added by me: cam
        //this is upon pressing return
        switch(PurchaseManager.instance.currentTitle)
        {
            case "Body":
                break;
            case "Engine":
                CarAnimator.instance.CloseHood();
                AudioBank.instance.PlaySound(snd.carHoodClose);
                break;
            case "Brakes":
                break;
            case "Tires":
                break;
        }



    }
    #endregion

    #region MenuSwapFunctions
        // Switch for GeneralMenu
    public bool ShowGeneralMenu(bool isOn)
    {
        if (isOn)
        {
            UpUI.basePanelHolder.SetActive(true);
            generalMenu.SetActive(true);
            returnBtn.gameObject.SetActive(false);
            GarageManager.instance.DefaultView();
            return true;
        }
        else
        {
            generalMenu.SetActive(false);
            returnBtn.gameObject.SetActive(true);
            return false;
        }
    }

    public bool ShowPurchasingMenu(bool isOn)
    {
        if (isOn)
        {
            UpUI.basePanelHolder.SetActive(false);
            UpUI.advPanelHolder.SetActive(true);
            return true;
        }
        else
        {
            UpUI.basePanelHolder.SetActive(true);
            UpUI.advPanelHolder.SetActive(false);
            return true;
        }
    }
    #endregion

    public void UpdateMoneyUI()
    {
        moneyText.text = GameManager.instance.money.ToString("C");
    }


}
