﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartPanelUI : MonoBehaviour
{

    [Header("Engine Texts")]
    [SerializeField] Text engineQuality;
    [SerializeField] Text engineHealth;
    [SerializeField] Text engineBoost;

    [Header("Body Texts")]
    [SerializeField] Text bodyQuality;
    [SerializeField] Text bodyHealth;
    [SerializeField] Text bodyBoost;

    [Header("Tires Texts")]
    [SerializeField] Text tiresQuality;
    [SerializeField] Text tiresHealth;
    [SerializeField] Text tiresBoost;

    [Header("Brakes Texts")]
    [SerializeField] Text brakesQuality;
    [SerializeField] Text brakesHealth;
    [SerializeField] Text brakesBoost;

    void Start()
    {
        //Debug.Log(CarStatus.instance.engineQuality);

        updateEngineUI();
        updateBodyUI();
        updateTireUI();
        updateBrakesUI();
    }

    void updateBodyUI()
    {
        bodyQuality.text = CarStatus.instance.bodyQuality.ToString() + " Quality";
        bodyHealth.text = "Durability: " + (CarStatus.instance.bodyHealth * 100).ToString("F0") + "%";
        bodyBoost.text = "Aerodynamics: " + (CarStatus.instance.currentBodyBoost * 100).ToString("F0") + "%";
    }

    void updateEngineUI()
    {
        engineQuality.text = CarStatus.instance.engineQuality.ToString() + " Quality";
        engineHealth.text = "Durability: " + (CarStatus.instance.engineHealth * 100).ToString("F0") + "%";
        engineBoost.text = "Torque: " + (CarStatus.instance.currentEngineBoost * 100).ToString("F0") + "%";
    }

    void updateBrakesUI()
    {
        brakesQuality.text = CarStatus.instance.brakesQuality.ToString() + " Quality";
        brakesHealth.text = "Durability: " + (CarStatus.instance.brakesHealth * 100).ToString("F0") + "%";
        brakesBoost.text = "Strength: " + (CarStatus.instance.currentBrakesBoost * 100).ToString("F0") + "%";
    }

    void updateTireUI()
    {
        tiresQuality.text = CarStatus.instance.tiresQuality.ToString() + " Quality";
        tiresHealth.text = "Durability: " + (CarStatus.instance.tiresHealth * 100).ToString("F0") + "%";
        tiresBoost.text = "Grip: " + (CarStatus.instance.currentTiresBoost * 100).ToString("F0") + "%";
    }
}
