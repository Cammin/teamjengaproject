﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarModel : MonoBehaviour
{


    public GameObject rustyBodyModel;
    public GameObject rustyEngineModel;
    public GameObject rustyBrakesModel;
    public GameObject rustyTiresModel;

    public GameObject standardBodyModel;
    public GameObject standardEngineModel;
    public GameObject standardBrakesModel;
    public GameObject standardTiresModel;

    public GameObject heavyBodyModel;
    public GameObject heavyEngineModel;
    public GameObject heavyBrakesModel;
    public GameObject heavyTiresModel;

    public GameObject sportsBodyModel;
    public GameObject sportsEngineModel;
    public GameObject sportsBrakesModel;
    public GameObject sportsTiresModel;

    public GameObject royalBodyModel;
    public GameObject royalEngineModel;
    public GameObject royalBrakesModel;
    public GameObject royalTiresModel;

    private void Start()
    {
        CarStatus.instance.LoadModel();
    }

}
