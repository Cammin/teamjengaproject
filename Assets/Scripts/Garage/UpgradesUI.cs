﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradesUI : MonoBehaviour
{
    [Header("Panels")]
    public GameObject basePanelHolder;
    public GameObject advPanelHolder;

    [Header("Buttons")]
    [SerializeField] Button engineUpgradeBtn;
    [SerializeField] Button bodyUpgradeBtn;
    [SerializeField] Button brakeUpgradeBtn;
    [SerializeField] Button tireUpgradeBtn;
    [SerializeField] Button nextBtn;
    [SerializeField] Button prevBtn;
    [SerializeField] Button purchaseBtn;

    [Header("Titles")]
    public Text purchaseUpgradeTitle;
    public Text currentUgradeTitle;
    [Header("Descriptions")]
    public Text currentUpgradeDescription;
    public Text qualityDescription;
    public Text itemDescription;
    public Text itemValues;

    [HideInInspector] public float price;
    [HideInInspector] public float durability;
    [HideInInspector] public float uniqueValue;
    [HideInInspector] public string qualityDescr;
    [HideInInspector] public string itemDescr;
    [HideInInspector] public string partTitle;

    #region OnStart
    // Use this for initialization
    private void Awake()
    {

        SetupDelegatesOnAwake();
    }

    void SetupDelegatesOnAwake()
    {
        engineUpgradeBtn.onClick.AddListener(OnViewEngineUpgrades);
        bodyUpgradeBtn.onClick.AddListener(OnViewBodyUpgrades);
        brakeUpgradeBtn.onClick.AddListener(OnViewBrakeUpgrades);
        tireUpgradeBtn.onClick.AddListener(OnViewTireUpgrades);
        purchaseBtn.onClick.AddListener(OnPurchase);

        nextBtn.onClick.AddListener(OnNext);
        prevBtn.onClick.AddListener(OnPrevious);
    }
    #endregion

    #region ButtonCommands
    void OnViewBodyUpgrades() // BODY BUTTON
    {
        PurchaseManager.instance.currentTitle = "Body";
        PurchaseManager.instance.Show<Body>();
    }
    void OnViewEngineUpgrades() // ENGINE BUTTON
    {
        PurchaseManager.instance.currentTitle = "Engine";
        PurchaseManager.instance.Show<Engine>();
    }
    void OnViewBrakeUpgrades() // BRAKE BUTTON
    {
        PurchaseManager.instance.currentTitle = "Brakes";
        PurchaseManager.instance.Show<Brakes>();
    }
    void OnViewTireUpgrades() // TIRE BUTTON
    {
        PurchaseManager.instance.currentTitle = "Tires";
        PurchaseManager.instance.Show<Tires>();
    }
    void OnPurchase()
    {
        if(GameManager.instance.money >= price)
        {
            AudioBank.instance.PlaySound(snd.upgradeBought);
            PurchaseManager.instance.PurchasePart();

            GameManager.instance.UpdateMoney(price);
        }
        else
        {
            AudioBank.instance.PlaySound(snd.upgradeDenied);
            Debug.Log( "NOT ENOUGH MONEY!!");
        }
    }
    void OnNext() // NEXT BUTTON
    {
        if (PurchaseManager.instance.currentPart.partQuality >= PartQuality.Royal)
        {
            PurchaseManager.instance.currentPart.partQuality = 0;
        }
        else
        {
            PurchaseManager.instance.currentPart.partQuality++;
        }
        PurchaseManager.instance.currentPart.ViewPartQualities();
    }
    void OnPrevious() // PREVIOUS BUTTON
    {
        if (PurchaseManager.instance.currentPart.partQuality <= PartQuality.Rusty)
        {
            PurchaseManager.instance.currentPart.partQuality = PartQuality.Royal;
        }
        else
        {
            PurchaseManager.instance.currentPart.partQuality--;
        }
        PurchaseManager.instance.currentPart.ViewPartQualities();
    }
    #endregion

    #region PanelText

    public void UpdatePurchaseTemplate(string partName, string partDescription)
    {
        partTitle = partName;
        itemDescr = partDescription;

        currentUgradeTitle.text = "Current Part: " + partName + " " + PurchaseManager.instance.currentPart.equippedQuality;
        currentUpgradeDescription.text = PurchaseManager.instance.currentPart.equippedQuality.ToString();
        currentUpgradeDescription.text = itemDescr;
    }

    public void UpdatePurchaseDisplay(string qualityDescrip, PartQuality qualTitle, float qualDurability, float newPrice, float uniqueValue)
    {
        purchaseUpgradeTitle.text = partTitle + qualTitle.ToString();

        qualityDescription.text = qualityDescrip;

        price = newPrice;
        itemValues.text = "Durability: " + durability + "\n" +
                           "Value :" + uniqueValue + "\n" +
                           "Price: " + "$" + price;

    }
    public void UpdateCurrentUpgradePanel()
    {
        currentUgradeTitle.text = "Current Part: " + PurchaseManager.instance.currentPart.ToString() + PurchaseManager.instance.currentPart.partQuality.ToString();
    }
    #endregion
}
