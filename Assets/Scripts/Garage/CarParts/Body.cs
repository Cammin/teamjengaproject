﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Body : BaseCarPart
{
    public override void ViewPartQualities()
    {
        GarageManager.instance.BodyView();
        upgrUI.UpdatePurchaseTemplate(PurchaseManager.instance.currentTitle, GarageDescriptions.instance.bodyDescription);
        switch (partQuality)
        {
            case PartQuality.Rusty:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.rustyDescription, PartQuality.Rusty, PartValues.instance.rustyDurabilityOnBuy,
                                             PartValues.instance.rustyBodyPrice, boost.rustyBodyBoost);
                break;
            case PartQuality.Standard:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.standardDescription, PartQuality.Standard, PartValues.instance.standardDurabilityOnBuy,
                                             PartValues.instance.standardBodyPrice, boost.standardBodyBoost);
                break;
            case PartQuality.Sporty:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.sportyDescription, PartQuality.Sporty, PartValues.instance.sportsDurabilityOnBuy,
                                             PartValues.instance.sportsBodyPrice, boost.sportsBodyBoost);
                break;
            case PartQuality.Heavy:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.heavyDescription, PartQuality.Heavy, PartValues.instance.heavyDurabilityOnBuy,
                                             PartValues.instance.heavyBodyPrice, boost.heavyBodyBoost);
                break;
            case PartQuality.Royal:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.royalDescription, PartQuality.Royal, PartValues.instance.royaltyDurabilityOnBuy,
                                             PartValues.instance.royaltyBodyPrice, boost.royalBodyBoost);
                break;
        }
    }
}
