﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engine : BaseCarPart
{
    public override void ViewPartQualities()
    {
        upgrUI.UpdatePurchaseTemplate(PurchaseManager.instance.currentTitle, GarageDescriptions.instance.engineDescription);
        GarageManager.instance.EngineView();
        CarAnimator.instance.OpenHood();
        AudioBank.instance.PlaySound(snd.carHoodOpen);
        switch (partQuality)
        {
            case PartQuality.Rusty:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.rustyDescription, PartQuality.Rusty, PartValues.instance.rustyDurabilityOnBuy,
                                             PartValues.instance.rustyEnginePrice, boost.rustyEngineBoost);
                break;
            case PartQuality.Standard:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.standardDescription, PartQuality.Standard, PartValues.instance.standardDurabilityOnBuy,
                                             PartValues.instance.standardEnginePrice, boost.standardEngineBoost);
                break;
            case PartQuality.Sporty:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.sportyDescription, PartQuality.Sporty, PartValues.instance.sportsDurabilityOnBuy,
                                             PartValues.instance.sportsEnginePrice, boost.sportsEngineBoost);
                break;
            case PartQuality.Heavy:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.heavyDescription, PartQuality.Heavy, PartValues.instance.heavyDurabilityOnBuy,
                                             PartValues.instance.heavyEnginePrice, boost.heavyEngineBoost);
                break;
            case PartQuality.Royal:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.royalDescription, PartQuality.Royal, PartValues.instance.royaltyDurabilityOnBuy,
                                             PartValues.instance.royaltyEnginePrice, boost.royalEngineBoost);
                break;
        }
    }
}
