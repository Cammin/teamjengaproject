﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tires : BaseCarPart
{
    public override void ViewPartQualities()
    {
        GarageManager.instance.TiresView();
        upgrUI.UpdatePurchaseTemplate(PurchaseManager.instance.currentTitle, GarageDescriptions.instance.tiresDescription);
        switch (partQuality)
        {
            case PartQuality.Rusty:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.rustyDescription, PartQuality.Rusty, PartValues.instance.rustyDurabilityOnBuy,
                                             PartValues.instance.rustyTiresPrice, boost.rustyTiresBoost);
                break;
            case PartQuality.Standard:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.standardDescription, PartQuality.Standard, PartValues.instance.standardDurabilityOnBuy,
                                             PartValues.instance.standardTiresPrice, boost.standardTiresBoost);
                break;
            case PartQuality.Sporty:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.sportyDescription, PartQuality.Sporty, PartValues.instance.sportsDurabilityOnBuy,
                                             PartValues.instance.sportsTiresPrice, boost.sportsTiresBoost);
                break;
            case PartQuality.Heavy:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.heavyDescription, PartQuality.Heavy, PartValues.instance.heavyDurabilityOnBuy,
                                             PartValues.instance.heavyTiresPrice, boost.heavyTiresBoost);
                break;
            case PartQuality.Royal:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.royalDescription, PartQuality.Royal, PartValues.instance.royaltyDurabilityOnBuy,
                                             PartValues.instance.royaltyTiresPrice, boost.royalTiresBoost);
                break;
        }
    }
}
