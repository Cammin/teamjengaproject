﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brakes : BaseCarPart
{
    public override void ViewPartQualities()
    {
        GarageManager.instance.BrakesView();
        upgrUI.UpdatePurchaseTemplate(PurchaseManager.instance.currentTitle, GarageDescriptions.instance.brakesDescription);
        switch (partQuality)
        {
            case PartQuality.Rusty:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.rustyDescription, PartQuality.Rusty, PartValues.instance.rustyDurabilityOnBuy,
                                             PartValues.instance.rustyBrakesPrice, boost.rustyBrakesBoost);
                break;
            case PartQuality.Standard:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.standardDescription, PartQuality.Standard, PartValues.instance.standardDurabilityOnBuy,
                                             PartValues.instance.standardBrakesPrice, boost.standardBrakesBoost);
                break;
            case PartQuality.Sporty:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.sportyDescription, PartQuality.Sporty, PartValues.instance.sportsDurabilityOnBuy,
                                             PartValues.instance.sportsBrakesPrice, boost.sportsBrakesBoost);
                break;
            case PartQuality.Heavy:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.heavyDescription, PartQuality.Heavy, PartValues.instance.heavyDurabilityOnBuy,
                                             PartValues.instance.heavyBrakesPrice, boost.heavyBrakesBoost);
                break;
            case PartQuality.Royal:
                upgrUI.UpdatePurchaseDisplay(GarageDescriptions.instance.royalDescription, PartQuality.Royal, PartValues.instance.royaltyDurabilityOnBuy,
                                             PartValues.instance.royaltyBrakesPrice, boost.royalBrakesBoost);
                break;
        }
    }
}
