﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCarPart : MonoBehaviour
{

    public PartQuality equippedQuality;
    public PartQuality partQuality;

    public bool isBeingViewed;
    public UpgradesUI upgrUI;
    public BoostStats boost;

    public abstract void ViewPartQualities();

    public virtual void Awake()
    {
        Debug.Log("HDHDH");
        upgrUI = FindObjectOfType<UpgradesUI>();
        boost = CarStatus.instance.boost;
    }
}

