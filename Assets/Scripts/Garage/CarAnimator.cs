﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarAnimator : Singleton<CarAnimator>
{

    Animator anim;
    public string hoodOpenName;
    public string hoodCloseName;
    public string hoodWheelChangeName;

    // Use this for initialization
    void Start ()
    {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void OpenHood()
    {
        anim.Play(hoodOpenName);
    }

    public void CloseHood()
    {
        anim.Play(hoodCloseName);
    }

}
