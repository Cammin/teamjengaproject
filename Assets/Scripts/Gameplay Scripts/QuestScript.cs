﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DeliveryState
{
    none,
    retrieving,
    delivering
}

public class QuestScript : Singleton<QuestScript>
{
    public float baseReward = 15.0f;

    [Range(0.0f, 100.0f)]
    public float percentRewardDeviation = 25.0f;

    [Range(0.0f, 1.0f)]
    public float rewardDistanceModifier = 1.0f;

    [Range(0.0f, 50.0f)]
    public float rewardDeductionRateSeconds = 1.0f;

    [HideInInspector]
    public float currentReward;
    private float minimumReward;
    [Range(0.0f, 0.90f)]
    public float minimumRewardPercent = 0.25f;

    public CarDestination[] places;
    public CarPizzaPlace pizzaPlace;

    [HideInInspector]
    public DeliveryState deliveryState = DeliveryState.none;

    // Update is called once per frame
    void Update ()
    {
        TryMission();
	}

    void AssignSpots()
    {
        places = FindObjectsOfType<CarDestination>();
        if (places.Length > 0)
        {
            Debug.Log(places.Length.ToString() + " destinations exist in the scene.");
        }
        else
        {
            Debug.LogError("No destinations exist in the scene!");
        }

        pizzaPlace = FindObjectOfType<CarPizzaPlace>();
        if (pizzaPlace != null)
        {
            Debug.Log("The pizza place was found.");
        }
        else
        {
            Debug.LogError("No pizza place exists in the scene!");
        }
    }

    private void TryMission()
    {
        //if scene is only gameplay
        if (SceneManagement.instance.currentScene == SceneManagement.instance.gameplaySceneName && SceneManagement.instance.CheckScene("GameScene"))
        {
            if (deliveryState == DeliveryState.none)
            {
                AssignSpots();
                deliveryState = DeliveryState.delivering;
                //Debug.Log("doing quest!");
                SetMission();
                
            }

            if (deliveryState == DeliveryState.delivering)
            {
                DecreaseReward();
            }
        }
        else if (SceneManagement.instance.currentScene == SceneManagement.instance.tutorialSceneName && GameManager.instance.startedTutorial)
        {
            AssignSpots();
            if (deliveryState == DeliveryState.none)
            {
                deliveryState = DeliveryState.retrieving;
                SetMission();
            }
        }
    }

    public void SetMission()
    {
        SetRendering(null);
        if (deliveryState == DeliveryState.delivering)
        {
            PickDeliveryPlace();
        }
        else
        {// the only other possible case is to go back to the pizza place and retrieve a new pizza.
            pizzaPlace.isDestination = true;
            SetDestination(pizzaPlace.gameObject);
        }
    }

    void PickDeliveryPlace()
    {
        //we will generate a random number to to pick a place to deliver to.
        int pickedIndex = UnityEngine.Random.Range(0, places.Length);
        
        //selects a destination at random. 
        if (places[pickedIndex] != null)
        {
            CarDestination place = places[pickedIndex].GetComponent<CarDestination>();

            //If it doesnt pass the filter, then we repeat this function, recursive cluster style. >:D
            if (place.tier == GameManager.instance.currentGarageTier)
            {
                //passed through filter
                place.isDestination = true;
                SetDestination(place.gameObject);
                GenerateReward(place.transform.position);
            }
            else
            {
                PickDeliveryPlace();
                //Debug.Log("repeated the recursive cluster");
            }
        }
        else
        {
            Debug.LogError("Destination will not be picked; doesnt exist.");
            return;
        }
    }

    void SetDestination(GameObject point)
    {
        //Set the destination arrow's new target
        DestinationArrow arrow = FindObjectOfType<DestinationArrow>();
        arrow.destination = point.transform;

        //set the new object as the one who shall be rendered.
        SetRendering(point);

        //clean up the array of objects.
        //Array.Clear(places, 0, places.Length);
    }

    void GenerateReward(Vector3 destination)
    {
        float randomness = 1 + UnityEngine.Random.Range(-percentRewardDeviation * 0.01f, percentRewardDeviation * 0.01f);
        Vector3 carPosition = FindObjectOfType<SimpleCarController>().transform.position;
        float distance = Vector3.Distance(carPosition, destination);

        currentReward = (baseReward + (rewardDistanceModifier * distance)) * randomness;
        minimumReward = currentReward * minimumRewardPercent;
    }

    void DecreaseReward()
    {
        currentReward -= rewardDeductionRateSeconds * Time.deltaTime;
        currentReward = Mathf.Max(currentReward, minimumReward);
    }

    void SetRendering(GameObject obj)
    {
        //Disable all destination markers 
        foreach (CarDestination point in places)
        {
            point.GetComponent<Renderer>().enabled = false;
        }
        pizzaPlace.GetComponent<Renderer>().enabled = false;

        //if an object was specified, then it shall be rendered!
        if (obj != null)
        {
            obj.GetComponent<Renderer>().enabled = true;
        }
    }
}
