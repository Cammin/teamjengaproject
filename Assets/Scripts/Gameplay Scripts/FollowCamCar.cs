﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamCar : MonoBehaviour
{
    public Transform carObject;
    public Transform carCamPivot;
    public Vector3 cameraOffset;
    public float cameraFollowSpeed;
    public float angleOfDepression;

    //public bool translatingEnabled = true;
    //public bool rotatingEnabled = true;

    public bool xRotateIgnored = false;
    public bool yRotateIgnored = false;
    public bool zRotateIgnored = false;

    void Start ()
    {
        carCamPivot.transform.localPosition = new Vector3(0, 0, cameraOffset.z);
        TransformUpdate(true);
        RotateUpdate(true);
    }

    void FixedUpdate ()
    {
        carCamPivot.transform.localPosition = new Vector3(0, 0, cameraOffset.z);
        TransformUpdate(false);
        RotateUpdate(false);
    }

    void TransformUpdate(bool initial)
    {
        Vector3 currentPosition = transform.position;
        Vector3 targetPosition = new Vector3(carCamPivot.position.x + cameraOffset.x, carObject.position.y + cameraOffset.y, carCamPivot.position.z);

        if (initial)
        {
            transform.position = targetPosition;
        }
        else
        {
            Vector3 lerpPosition = Vector3.Lerp(currentPosition, targetPosition, cameraFollowSpeed);
            transform.position = lerpPosition;
        }
    }

    void RotateUpdate(bool initial)
    {
        Vector3 depression = new Vector3(angleOfDepression, 0, 0);
        Vector3 currentAngle = transform.eulerAngles;
        Vector3 targetAngle = carCamPivot.eulerAngles;

        float targetX;
        float targetY;
        float targetZ;

        #region AxisIgnoring
        if (xRotateIgnored)
        {
            targetX = currentAngle.x;
        }
        else
        {
            targetX = targetAngle.x;
        }

        if (yRotateIgnored)
        {
            targetY = currentAngle.y;
        }
        else
        {
            targetY = targetAngle.y;
        }

        if (zRotateIgnored)
        {
            targetZ = currentAngle.z;
        }
        else
        {
            targetZ = targetAngle.z;
        }
        #endregion

        if (initial)
        {
            //transform.LookAt(carObject, Vector3.up);
            transform.eulerAngles = targetAngle + depression;
        }
        else
        {
            Vector3 lerpAngle = new Vector3(Mathf.LerpAngle(currentAngle.x, targetX, cameraFollowSpeed),
                                            Mathf.LerpAngle(currentAngle.y, targetY, cameraFollowSpeed),
                                            Mathf.LerpAngle(currentAngle.z, targetZ, cameraFollowSpeed));
            transform.eulerAngles = lerpAngle;
        }
    }
}
