﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : Singleton<ObstacleManager>
{
    [Range(0, 100)]
    public float basePercentChancePerSecond = 1;
    public float daysLivedMultiplier = 1;
    public List<ObstacleObject> obstacles = new List<ObstacleObject>();

	public void StartDay ()
    {
        StartCoroutine(TryFall());

        obstacles.AddRange(FindObjectsOfType<ObstacleObject>());
        Debug.Log("Located " + obstacles.Count.ToString() + " obstacles. Chance to fell an obstacle per second is " + PercentChance().ToString() + "%");
	}

    IEnumerator TryFall()
    {
        yield return new WaitForSecondsRealtime(1.0f);

        if (SceneManagement.instance.currentScene == SceneManagement.instance.gameplaySceneName)
        {
            if(RandomChance())
            {
                MakeAnObjectFall();
            }
            StartCoroutine(TryFall());
        }
        else
        {
            obstacles.Clear();
        }
    }

    bool RandomChance()
    {
        bool willDo = false;
            
        if (Random.Range(0, 100) <= PercentChance())
        {
            willDo = true;
        }

        return willDo;
    }

    float PercentChance()
    {
        return basePercentChancePerSecond + GameManager.instance.daysLived * daysLivedMultiplier;
    }
    
    void MakeAnObjectFall()
    {
        if (obstacles.Count > 0)
        {
            int i = Random.Range(0, obstacles.Count);
            obstacles[i].Fall();
            //obstacles.Remove(obstacles[i]);

            Debug.Log("Fell an obstacle");
        }
        else
        {
            Debug.Log("Tried falling an obstacle but all are fallen!");
        }
    }
}
