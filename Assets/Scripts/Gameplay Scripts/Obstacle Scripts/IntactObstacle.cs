﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntactObstacle : MonoBehaviour
{
    CarCollision carCollision;

    private void Start()
    {
        if (FindObjectOfType<CarCollision>() != null)
        {
            carCollision = FindObjectOfType<CarCollision>().GetComponent<CarCollision>();
        }
        else
        {
            Debug.LogError("CarCollision script not found!");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (carCollision.currentMagnitude > CarStatus.instance.impactTolerance)
        {
            GetComponentInParent<ObstacleObject>().Fall();
        }
    }
}
