﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleObject : MonoBehaviour
{
    GameObject intactObject;
    GameObject fallenObject;

    void Start ()
    {
        intactObject = GetComponentInChildren<IntactObstacle>().gameObject;
        fallenObject = GetComponentInChildren<FallenObstacle>().gameObject;

        intactObject.SetActive(true);
        fallenObject.SetActive(false);
    }
	
    public void Fall()
    {
        intactObject.SetActive(false);
        fallenObject.SetActive(true);
        ObstacleManager.instance.obstacles.Remove(gameObject.GetComponent<ObstacleObject>());
    }
}
