﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinationArrow : MonoBehaviour
{
    [HideInInspector]
    public Transform destination;

    [Range(0.0f, 1.0f)]
    public float maxAlpha = 0.5f;
    public float fadeFarDistance = 10;
    public float fadedCloseDistance = 5;
    public float targetPoint;

    [HideInInspector]
    public float distance;


    void Update()
    {
        if (destination != null)
        {
            FollowTarget();
        }
    }

    void SetDestination()
    {
        //dest = FindObjectOfType<CarDestination>();
        //if (dest == null)
        {
            Debug.LogError("There are no car destinations!");
        }
    }

    void FollowTarget()
    {
        transform.LookAt(destination, Vector3.up);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

        distance = Vector3.Distance(transform.position, destination.position);

        if (distance >= fadeFarDistance) //further than range
        {
            SetFade(maxAlpha);
        }
        else if (distance > fadedCloseDistance && distance < fadeFarDistance) //within range
        {
            float startValue = fadedCloseDistance;
            float currentValue = distance - startValue;
            float endValue = fadeFarDistance - fadedCloseDistance;

            float progress = Mathf.Lerp(0, maxAlpha, currentValue / endValue);

            SetFade(progress);
        }
        else if (distance <= fadedCloseDistance) //inside dead zone
        {
            SetFade(0);
        }


    }

    void SetFade(float progress)
    {
        Material mat = GetComponentInChildren<MeshRenderer>().material;

        mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, progress);
    }

}
