﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarDestination : MonoBehaviour
{
    public Tier tier;
    public float spinSpeed = 5;

    [HideInInspector]
    public bool isDestination = false;

    private void Update()
    {
        transform.eulerAngles += new Vector3(0, spinSpeed * Time.deltaTime, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<SimpleCarController>() == true && isDestination == true)
        {
            isDestination = false;
            QuestScript.instance.deliveryState = DeliveryState.retrieving;
            QuestScript.instance.SetMission();

            AudioBank.instance.PlaySound(snd.moneyCollected);

            if (GameManager.instance.startedTutorial)
            {
                TutorialScript tut = FindObjectOfType<TutorialScript>();

                tut.tutorialPing = true;

                Destroy(gameObject);
                
            }
            else
            {
                //reward money
                GameManager.instance.money += QuestScript.instance.currentReward;
            }
        }
    }
}
