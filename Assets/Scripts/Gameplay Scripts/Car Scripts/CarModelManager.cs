﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarModelManager : MonoBehaviour
{
    public GameObject rustyBody;
    public GameObject standardBody;
    public GameObject heavyBody;
    public GameObject sportsBody;
    public GameObject royalBody;

    [Space(5)]

    public GameObject rustyEngine;
    public GameObject standardEngine;
    public GameObject heavyEngine;
    public GameObject sportsEngine;
    public GameObject royalEngine;

    [Space(5)]

    public GameObject rustyBrakes;
    public GameObject standardBrakes;
    public GameObject heavyBrakes;
    public GameObject sportsBrakes;
    public GameObject royalBrakes;

    [Space(5)]

    public GameObject rustyTires;
    public GameObject standardTires;
    public GameObject heavyTires;
    public GameObject sportsTires;
    public GameObject royalTires;

    void Start ()
    {
        ChangeModelAndMaterial();

    }
	
    private void ChangeModelAndMaterial()
    {
        //Unload all models
        rustyEngine.SetActive(false);
        standardEngine.SetActive(false);
        heavyEngine.SetActive(false);
        sportsEngine.SetActive(false);
        royalEngine.SetActive(false);

        rustyBrakes.SetActive(false);
        standardBrakes.SetActive(false);
        heavyBrakes.SetActive(false);
        sportsBrakes.SetActive(false);
        royalBrakes.SetActive(false);

        rustyTires.SetActive(false);
        standardTires.SetActive(false);
        heavyTires.SetActive(false);
        sportsTires.SetActive(false);
        royalTires.SetActive(false);

        //Set all models
        switch (CarStatus.instance.bodyQuality)
        {
            case PartQuality.Rusty:
                rustyBody.SetActive(true);
                break;

            case PartQuality.Standard:
                standardBody.SetActive(true);
                break;

            case PartQuality.Heavy:
                heavyBody.SetActive(true);
                break;

            case PartQuality.Sporty:
                sportsBody.SetActive(true);
                break;

            case PartQuality.Royal:
                royalBody.SetActive(true);
                break;

            default:
                break;
        }

        switch (CarStatus.instance.engineQuality)
        {
            case PartQuality.Rusty:
                rustyEngine.SetActive(true);
                break;

            case PartQuality.Standard:
                standardEngine.SetActive(true);
                break;

            case PartQuality.Heavy:
                heavyEngine.SetActive(true);
                break;

            case PartQuality.Sporty:
                sportsEngine.SetActive(true);
                break;

            case PartQuality.Royal:
                royalEngine.SetActive(true);
                break;

            default:
                break;
        }

        switch (CarStatus.instance.brakesQuality)
        {
            case PartQuality.Rusty:
                rustyBrakes.SetActive(true);
                break;

            case PartQuality.Standard:
                standardBrakes.SetActive(true);
                break;

            case PartQuality.Heavy:
                heavyBrakes.SetActive(true);
                break;

            case PartQuality.Sporty:
                sportsBrakes.SetActive(true);
                break;

            case PartQuality.Royal:
                royalBrakes.SetActive(true);
                break;

            default:
                break;
        }

        switch (CarStatus.instance.tiresQuality)
        {
            case PartQuality.Rusty:
                rustyTires.SetActive(true);
                break;

            case PartQuality.Standard:
                standardTires.SetActive(true);
                break;

            case PartQuality.Heavy:
                heavyTires.SetActive(true);
                break;

            case PartQuality.Sporty:
                sportsTires.SetActive(true);
                break;

            case PartQuality.Royal:
                royalTires.SetActive(true);
                break;

            default:
                break;
        }
    }
}
