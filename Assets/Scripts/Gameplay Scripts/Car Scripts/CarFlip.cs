﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarFlip : MonoBehaviour
{
    public float timeTillFlip = 3f;
    private float timeTemp;
    public float heightReset = 2f;

    public

	// Use this for initialization
	void Start ()
    {
        timeTemp = timeTillFlip;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Vector3.Dot(transform.up, Vector3.down) > -0.1) //
        {
            timeTemp -= Time.fixedDeltaTime;
            Debug.Log("Will flip back in: " + timeTemp.ToString("F1"));

            if (timeTemp <= 0)
            {
                //teleport car back
                if (Camera.main != null)
                {
                    transform.position += new Vector3(0, heightReset, 0);
                    transform.eulerAngles = new Vector3(0, Camera.main.transform.eulerAngles.y, 0);
                }
                else
                {
                    Debug.Log("Camera is not tagged as MainCamera!");
                }
            }
        }
        else
        {
            timeTemp = timeTillFlip;
        }
    }
}
