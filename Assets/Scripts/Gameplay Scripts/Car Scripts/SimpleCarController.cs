﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class AxleInfo
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor;
    public bool steering;
}

public class SimpleCarController : MonoBehaviour
{
    public List<AxleInfo> axleInfos;
    private CarStatus stats;

    private void Start()
    {
        stats = CarStatus.instance;
        GetComponent<Rigidbody>().centerOfMass = new Vector3(0, stats.centerMassY, 0);
    }




    // finds the corresponding visual wheel
    // correctly applies the transform
    public void ApplyLocalPositionToVisuals(WheelCollider collider)
    {
        if (collider.transform.childCount == 0)
        {
            return;
        }

        Transform visualWheel = collider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }

    public void FixedUpdate()
    {
        //apply a passive force down onto the car to stick to the ground
        //GetComponent<Rigidbody>().AddForce(Vector3.down, ForceMode.Acceleration);

        #region Steering&Brakes
        float motorDir = Input.GetAxis("Vertical");
        float motorPower = 0;
        float brakesPower = 0;

        if (motorDir != 0)
        {
            motorPower = stats.currentMotorTorque * motorDir;
            CarStatus.instance.DoEngineDamage();
        }

        if (Input.GetKey(KeyCode.Space))
        {
            brakesPower = CarStatus.instance.currentBrakeTorque;
            CarStatus.instance.DoBrakesDamage();
        }
        #endregion

        float targetSteeringAngle = Input.GetAxis("Horizontal") * stats.maxSteeringAngle;
        //This one will affect friction too in the future if we can figure it out.
        float turningSpeed = stats.baseTurningSpeed;// * CarStatus.instance.tiresFactor;

        if (stats.currentSteeringAngle > targetSteeringAngle)
        {//turning left
            stats.currentSteeringAngle -= turningSpeed * Time.fixedDeltaTime;

            //snap in case it moves too far off the angle
            if (stats.currentSteeringAngle < targetSteeringAngle)
            {
                stats.currentSteeringAngle = targetSteeringAngle;
            }
        }

        if (stats.currentSteeringAngle < targetSteeringAngle)
        {//turning right
            stats.currentSteeringAngle += turningSpeed * Time.fixedDeltaTime;

            //snap in case it moves too far off the angle
            if (stats.currentSteeringAngle > targetSteeringAngle)
            {
                stats.currentSteeringAngle = targetSteeringAngle;
            }
        }

        //print("STATS CURR STEER ANGLE: " + stats.currentSteeringAngle + ", TARGET ANGLE: " + targetSteeringAngle);



        foreach (AxleInfo axleInfo in axleInfos)
        {
            if (axleInfo.steering == true)
            {
                axleInfo.leftWheel.steerAngle = stats.currentSteeringAngle;
                axleInfo.rightWheel.steerAngle = stats.currentSteeringAngle;
            }

            if (axleInfo.motor == true)
            {
                axleInfo.leftWheel.motorTorque = motorPower;
                axleInfo.rightWheel.motorTorque = motorPower;

                axleInfo.leftWheel.brakeTorque = brakesPower;
                axleInfo.rightWheel.brakeTorque = brakesPower;

                float tiresAverageDeltaRotationSpeed = 0;
                tiresAverageDeltaRotationSpeed += axleInfo.leftWheel.rpm;
                tiresAverageDeltaRotationSpeed += axleInfo.rightWheel.rpm;
                tiresAverageDeltaRotationSpeed /= 2.0f;
                CarStatus.instance.tiresAverageRPM = tiresAverageDeltaRotationSpeed;
            }

            ApplyLocalPositionToVisuals(axleInfo.leftWheel);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel);

            WheelFrictionCurve tiresFrictionForwardsCurve;
            WheelFrictionCurve tiresFrictionSidewaysCurve;

            //All of the stats are being based off of the FRONT LEFT wheel, except for the ones set by the carStatus.

            tiresFrictionForwardsCurve = axleInfo.leftWheel.forwardFriction;
            tiresFrictionSidewaysCurve = axleInfo.leftWheel.sidewaysFriction;

            tiresFrictionForwardsCurve.asymptoteValue = stats.currentTiresForwardsFriction;
            tiresFrictionSidewaysCurve.asymptoteValue = stats.currentTiresSidewaysFriction;
            tiresFrictionForwardsCurve.extremumValue = stats.currentTiresForwardsFriction * stats.extremumMultiplier;
            tiresFrictionSidewaysCurve.extremumValue = stats.currentTiresSidewaysFriction * stats.extremumMultiplier;

            tiresFrictionForwardsCurve.asymptoteSlip = stats.asymptoteSlip;
            tiresFrictionSidewaysCurve.asymptoteSlip = stats.asymptoteSlip;
            tiresFrictionForwardsCurve.extremumSlip = stats.extremumSlip;
            tiresFrictionSidewaysCurve.extremumSlip = stats.extremumSlip;

            axleInfo.leftWheel.forwardFriction = tiresFrictionForwardsCurve;
            axleInfo.rightWheel.forwardFriction = tiresFrictionForwardsCurve;

            axleInfo.leftWheel.sidewaysFriction = tiresFrictionSidewaysCurve;
            axleInfo.rightWheel.sidewaysFriction = tiresFrictionSidewaysCurve;
        }
    }
}