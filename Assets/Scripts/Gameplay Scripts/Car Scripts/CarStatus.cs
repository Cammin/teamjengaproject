﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class BoostStats {
    [Header("Engine Types")]
    public float rustyEngineBoost = 1.0f;
    public float standardEngineBoost = 1.0f;
    public float heavyEngineBoost = 1.0f;
    public float sportsEngineBoost = 1.5f;
    public float royalEngineBoost = 2.0f;
    [Header("Body Types")]
    public float rustyBodyBoost = 1.0f;
    public float standardBodyBoost = 1.0f;
    public float heavyBodyBoost = 1.0f;
    public float sportsBodyBoost = 1.5f;
    public float royalBodyBoost = 2.0f;
    [Header("Brake Types")]
    public float rustyBrakesBoost = 1.0f;
    public float standardBrakesBoost = 1.0f;
    public float heavyBrakesBoost = 1.0f;
    public float sportsBrakesBoost = 1.5f;
    public float royalBrakesBoost = 2.0f;
    [Header("Tire Types")]
    public float rustyTiresBoost = 1.0f;
    public float standardTiresBoost = 1.0f;
    public float heavyTiresBoost = 1.0f;
    public float sportsTiresBoost = 1.5f;
    public float royalTiresBoost = 2.0f;
}

public class CarStatus : Singleton<CarStatus> {
    //these pertain to the actual car.
    [HideInInspector]
    public float currentMotorTorque;
    public float maxMotorTorque = 750;

    [HideInInspector]
    public float currentBrakeTorque;
    public float maxBrakeTorque = 750;

    [HideInInspector]
    public float currentTiresForwardsFriction; // asymptoteValue
    [HideInInspector]
    public float currentTiresSidewaysFriction; // asymptoteValue

    [Space(10)]
    public float maxTiresForwardsFriction; // max asymptoteValue
    public float maxTiresSidewaysFriction; // max asymptoteValue

    public float asymptoteSlip = 2;

    public float extremumSlip = 2;
    public float extremumMultiplier = 1;

    [Space(5)]
    public float centerMassY = -1;

    [Space(5)]
    [HideInInspector]
    public float currentSteeringAngle = 0;
    public float maxSteeringAngle;
    public float baseTurningSpeed;


    public Body body;
    public Engine engine;
    public Brakes brakes;
    public Tires tires;

    public PartQuality bodyQuality;
    public PartQuality engineQuality;
    public PartQuality brakesQuality;
    public PartQuality tiresQuality;

    [Space(5)]

    public CarModel model;

    [Space(5)]

    public BoostStats boost;

    public float currentBodyBoost = 1.0f;
    public float currentEngineBoost = 1.0f;
    public float currentBrakesBoost = 1.0f;
    public float currentTiresBoost = 1.0f;

    [Space(5)]
    public float impactTolerance = 5.0f;
    public float brakesRPMTolerance = 5.0f;
    public float tiresRPMTolerance = 5.0f;

    public float collisionBaseDamage = 1.0f;
    public float engineDamageRate = 1.0f;
    public float brakesDamageRate = 1.0f;
    public float tiresDamageRate = 1.0f;



    [Space(5)]

    [Range(0.0f, 4.0f)]
    public float bodyHealth = 1.0f;
    [Range(0.0f, 4.0f)]
    public float engineHealth = 1.0f;
    [Range(0.0f, 4.0f)]
    public float brakesHealth = 1.0f;
    [Range(0.0f, 4.0f)]
    public float tiresHealth = 1.0f;

    [Space(5)]

    [Header("The factors are read-only, keep at 0.")]
    public float bodyFactor;
    public float engineFactor;
    public float brakesFactor;
    public float tiresForwardsFactor;
    public float tiresSidewaysFactor;
    public float tiresAverageRPM = 0.0f;


    [Space(5)]


    [Header("This is the lowest performance point before health hits zero. when health is at ~1%, the performance is this strong.")]
    [Range(0.0f, 1.0f)]
    public float bodyDegradationThreshold = 0.5f;
    [Range(0.0f, 1.0f)]
    public float engineDegradationThreshold = 0.5f;
    [Range(0.0f, 1.0f)]
    public float brakesDegradationThreshold = 0.5f;
    [Range(0.0f, 1.0f)]
    public float tiresForwardDegradationThreshold = 0.5f;
    [Range(0.0f, 1.0f)]
    public float tiresSidewaysDegradationThreshold = 0.5f;

    private void Awake() {
        body = FindObjectOfType<Body>();
        engine = FindObjectOfType<Engine>();
        brakes = FindObjectOfType<Brakes>();
        tires = FindObjectOfType<Tires>();

    }

    private void Start() {


        body = FindObjectOfType<Body>();
        engine = FindObjectOfType<Engine>();
        tires = FindObjectOfType<Tires>();
        brakes = FindObjectOfType<Brakes>();
        CalculatePerformance();
        Debug.Log("update model is in update loop currently");
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (SceneManagement.instance.CheckScene(SceneManagement.instance.gameplaySceneName)) {
                SceneManagement.instance.ChangeScene(SceneManagement.instance.garageSceneName);
            }
        }
    }

    public void UponSceneChange() {



    }

    public void CalculatePerformance() {
        SetBoosts();

        //These make sure that the factor isn't overperforming if the car had health past 100%
        bodyFactor = Mathf.Min(bodyHealth, 1.0f);
        engineFactor = Mathf.Min(engineHealth, 1.0f);
        brakesFactor = Mathf.Min(brakesHealth, 1.0f);
        tiresForwardsFactor = Mathf.Min(tiresHealth, 1.0f);
        tiresSidewaysFactor = Mathf.Min(tiresHealth, 1.0f);

        //setting the durability when under 100%, down to the threshold.
        if (bodyHealth < 1.0f && bodyHealth > 0.0f) {
            bodyFactor = Mathf.Lerp(bodyDegradationThreshold, 1.0f, bodyHealth * currentBodyBoost);
        } else if (bodyHealth < 0) {
            bodyHealth = 0.0f;
        }

        //acceleration is impacted
        if (engineHealth < 1.0f && engineHealth > 0.0f) {
            engineFactor = Mathf.Lerp(engineDegradationThreshold, 1.0f, engineHealth) * currentEngineBoost;
        } else if (engineHealth < 0) {
            engineHealth = 0.0f;
        }

        //breaking power is impacted
        if (brakesHealth < 1.0f && brakesHealth > 0.0f) {
            brakesFactor = Mathf.Lerp(brakesDegradationThreshold, 1.0f, brakesHealth) * currentBrakesBoost;
        } else if (brakesHealth < 0) {
            brakesHealth = 0.0f;
        }

        //ease of forwardTurning is impacted
        if (tiresHealth < 1.0f && tiresHealth > 0.0f) {
            tiresForwardsFactor = Mathf.Lerp(tiresForwardDegradationThreshold, 1.0f, tiresHealth) * currentTiresBoost;
            tiresSidewaysFactor = Mathf.Lerp(tiresSidewaysDegradationThreshold, 1.0f, tiresHealth) * currentTiresBoost;
        } else if (tiresHealth < 0) {
            tiresHealth = 0.0f;
        }

        CalculateCarApplications();
    }

    private void SetBoosts() {
        switch (bodyQuality) {
            case PartQuality.Rusty:
                currentBodyBoost = boost.rustyBodyBoost;
                break;

            case PartQuality.Standard:
                currentBodyBoost = boost.standardBodyBoost;
                break;

            case PartQuality.Heavy:
                currentBodyBoost = boost.heavyBodyBoost;
                break;

            case PartQuality.Sporty:
                currentBodyBoost = boost.sportsBodyBoost;
                break;

            case PartQuality.Royal:
                currentBodyBoost = boost.royalBodyBoost;
                break;

            default:
                break;
        }

        switch (engineQuality) {
            case PartQuality.Rusty:
                currentEngineBoost = boost.rustyEngineBoost;
                break;

            case PartQuality.Standard:
                currentEngineBoost = boost.standardEngineBoost;
                break;

            case PartQuality.Heavy:
                currentEngineBoost = boost.heavyEngineBoost;
                break;

            case PartQuality.Sporty:
                currentEngineBoost = boost.sportsEngineBoost;
                break;

            case PartQuality.Royal:
                currentEngineBoost = boost.royalEngineBoost;
                break;

            default:
                break;
        }

        switch (brakesQuality) {
            case PartQuality.Rusty:
                currentBrakesBoost = boost.rustyBrakesBoost;
                break;

            case PartQuality.Standard:
                currentBrakesBoost = boost.standardBrakesBoost;
                break;

            case PartQuality.Heavy:
                currentBrakesBoost = boost.heavyBrakesBoost;
                break;

            case PartQuality.Sporty:
                currentBrakesBoost = boost.sportsBrakesBoost;
                break;

            case PartQuality.Royal:
                currentBrakesBoost = boost.royalBrakesBoost;
                break;

            default:
                break;
        }

        switch (tiresQuality) {
            case PartQuality.Rusty:
                currentTiresBoost = boost.rustyTiresBoost;
                break;

            case PartQuality.Standard:
                currentTiresBoost = boost.standardTiresBoost;
                break;

            case PartQuality.Heavy:
                currentTiresBoost = boost.heavyTiresBoost;
                break;

            case PartQuality.Sporty:
                currentTiresBoost = boost.sportsTiresBoost;
                break;

            case PartQuality.Royal:
                currentTiresBoost = boost.royalTiresBoost;
                break;

            default:
                break;
        }
    }

    void CalculateCarApplications() {
        //calculate actual car applied stats

        currentMotorTorque = maxMotorTorque * engineFactor * bodyFactor;
        currentBrakeTorque = maxBrakeTorque * brakesFactor * bodyFactor;
        currentTiresForwardsFriction = maxTiresForwardsFriction * tiresForwardsFactor * bodyFactor;
        currentTiresSidewaysFriction = maxTiresSidewaysFriction * tiresSidewaysFactor * bodyFactor;
    }

    public void LoadModel() {//first set all models to inactive
        model = FindObjectOfType<CarModel>();

        if (SceneManagement.instance.CheckScene("Garage")) {
            body = FindObjectOfType<Body>();
            engine = FindObjectOfType<Engine>();
            brakes = FindObjectOfType<Brakes>();
            tires = FindObjectOfType<Tires>();

            bodyQuality = body.equippedQuality;
            engineQuality = engine.equippedQuality;
            brakesQuality = brakes.equippedQuality;
            tiresQuality = tires.equippedQuality;
        }

        model.rustyBodyModel.SetActive(false);
        model.rustyEngineModel.SetActive(false);
        model.rustyBrakesModel.SetActive(false);
        model.rustyTiresModel.SetActive(false);
        model.standardBodyModel.SetActive(false);
        model.standardEngineModel.SetActive(false);
        model.standardBrakesModel.SetActive(false);
        model.standardTiresModel.SetActive(false);
        model.heavyBodyModel.SetActive(false);
        model.heavyEngineModel.SetActive(false);
        model.heavyBrakesModel.SetActive(false);
        model.heavyTiresModel.SetActive(false);
        model.sportsBodyModel.SetActive(false);
        model.sportsEngineModel.SetActive(false);
        model.sportsBrakesModel.SetActive(false);
        model.sportsTiresModel.SetActive(false);
        model.royalBodyModel.SetActive(false);
        model.royalEngineModel.SetActive(false);
        model.royalBrakesModel.SetActive(false);
        model.royalTiresModel.SetActive(false);

        //then set the specifics to active depending on what's equipped
        switch (bodyQuality) {
            case PartQuality.Rusty:
                model.rustyBodyModel.SetActive(true);
                break;
            case PartQuality.Standard:
                model.standardBodyModel.SetActive(true);
                break;
            case PartQuality.Heavy:
                model.heavyBodyModel.SetActive(true);
                break;
            case PartQuality.Sporty:
                model.sportsBodyModel.SetActive(true);
                break;
            case PartQuality.Royal:
                model.royalBodyModel.SetActive(true);
                break;
            default:
                break;
        }
        switch (engineQuality) {
            case PartQuality.Rusty:
                model.rustyEngineModel.SetActive(true);
                break;
            case PartQuality.Standard:
                model.standardEngineModel.SetActive(true);
                break;
            case PartQuality.Heavy:
                model.heavyEngineModel.SetActive(true);
                break;
            case PartQuality.Sporty:
                model.sportsEngineModel.SetActive(true);
                break;
            case PartQuality.Royal:
                model.royalEngineModel.SetActive(true);
                break;
            default:
                break;
        }
        switch (brakesQuality) {
            case PartQuality.Rusty:
                model.rustyBrakesModel.SetActive(true);
                break;
            case PartQuality.Standard:
                model.standardBrakesModel.SetActive(true);
                break;
            case PartQuality.Heavy:
                model.heavyBrakesModel.SetActive(true);
                break;
            case PartQuality.Sporty:
                model.sportsBrakesModel.SetActive(true);
                break;
            case PartQuality.Royal:
                model.royalBrakesModel.SetActive(true);
                break;
            default:
                break;
        }
        switch (tiresQuality) {
            case PartQuality.Rusty:
                model.rustyTiresModel.SetActive(true);
                break;
            case PartQuality.Standard:
                model.standardTiresModel.SetActive(true);
                break;
            case PartQuality.Heavy:
                model.heavyTiresModel.SetActive(true);
                break;
            case PartQuality.Sporty:
                model.sportsTiresModel.SetActive(true);
                break;
            case PartQuality.Royal:
                model.royalTiresModel.SetActive(true);
                break;
            default:
                break;
        }
    }


    public void DoBodyDamage(float magnitudeFactor) {
        if (GameManager.instance.hack.bodyInvincibility == false)
            bodyHealth -= collisionBaseDamage * magnitudeFactor; //also must be affected by the player's speed so that the damage is proper. also make this exponential of speed.
        //maybe create particles here, also various crash sounds with volume scaling with the crash
    }

    public void DoEngineDamage() {
        if (GameManager.instance.hack.engineInvincibility == false)
            engineHealth -= engineDamageRate * Time.fixedDeltaTime;
        //maybe do engine exhaust particles here, also sound
    }

    public void DoBrakesDamage() {
        if (GameManager.instance.hack.brakesInvincibility == false) {
            if (tiresAverageRPM > brakesRPMTolerance) {
                brakesHealth -= brakesDamageRate * Time.fixedDeltaTime; //also must be affected by the player's speed to seem realistic.
            }
        }
    }

    public void DoTiresDamage() {
        if (GameManager.instance.hack.tiresInvincibility == false) {
            if (tiresAverageRPM > tiresRPMTolerance) {
                tiresHealth -= tiresDamageRate * Time.fixedDeltaTime; //also must be affected by the player's speed to seem realistic.
            }
        }

    }
}
