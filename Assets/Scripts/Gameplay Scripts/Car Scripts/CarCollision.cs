﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarCollision : MonoBehaviour
{
    private CarStatus stats;
    //EngineBlendScript engine;

    [HideInInspector] public float currentMagnitude;
    [HideInInspector] public float currentSpeed;

    private Rigidbody rb;

    void Start ()
    {
        stats = CarStatus.instance;
        //engine = FindObjectOfType<EngineBlendScript>();
        rb = GetComponent<Rigidbody>();
    }

	void FixedUpdate()
    {
        float x = rb.velocity.x;
        float z = rb.velocity.z;
        currentSpeed = (x < 0 ? -x : x) + (z < 0 ? -z : z);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<NotCollidable>() == null && currentMagnitude > stats.impactTolerance)
        {
            AudioBank.instance.PlayImpact(currentMagnitude - stats.impactTolerance);
            stats.DoBodyDamage(currentMagnitude * 0.001f);
        }
    }


}
