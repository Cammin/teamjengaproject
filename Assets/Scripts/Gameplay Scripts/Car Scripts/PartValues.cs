﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartValues : Singleton<PartValues>
{
    #region QualityPrices
    [Header("Rusty Prices")]
    public float rustyBodyPrice = 12;
    public float rustyEnginePrice = 10;
    public float rustyBrakesPrice = 5;
    public float rustyTiresPrice = 7;

    [Header("Standard Prices")]
    public float standardBodyPrice = 11;
    public float standardEnginePrice = 11;
    public float standardBrakesPrice = 11;
    public float standardTiresPrice = 11;

    [Header("Heavy Prices")]
    public float heavyBodyPrice = 12;
    public float heavyEnginePrice = 12;
    public float heavyBrakesPrice = 12;
    public float heavyTiresPrice = 12;

    [Header("Sports Prices")]
    public float sportsBodyPrice = 13;
    public float sportsEnginePrice = 13;
    public float sportsBrakesPrice = 13;
    public float sportsTiresPrice = 13;

    [Header("Royal Prices")]
    public float royaltyBodyPrice = 14;
    public float royaltyEnginePrice = 14;
    public float royaltyBrakesPrice = 14;
    public float royaltyTiresPrice = 14;

    [Header("Health When Bought")]
    public float rustyDurabilityOnBuy = 1;
    public float standardDurabilityOnBuy = 1;
    public float heavyDurabilityOnBuy = 1.5f;
    public float sportsDurabilityOnBuy = 1.5f;
    public float royaltyDurabilityOnBuy = 2;
    #endregion

    ////use this function when you successfully purchase something
    //public void PartPurchase(PartQuality quality, UpgradeParts part)
    //{
    //    float healing = 0;
    //    // set the healing
    //    switch (quality)
    //    {
    //        case PartQuality.Rusty:
    //            healing = rustyDurabilityOnBuy;
    //            break;
    //        case PartQuality.Standard:
    //            healing = standardDurabilityOnBuy;
    //            break;
    //        case PartQuality.Heavy:
    //            healing = heavyDurabilityOnBuy;
    //            break;
    //        case PartQuality.Sporty:
    //            healing = sportsDurabilityOnBuy;
    //            break;
    //        case PartQuality.Royal:
    //            healing = royaltyDurabilityOnBuy;
    //            break;
    //    }
    //    switch (part) // set the healing to the specified part
    //    {
    //        case UpgradeParts.Body:
    //            CarStatus.instance.bodyHealth = healing;
    //            CarStatus.instance.bodyQuality = quality;
    //            break;
    //        case UpgradeParts.Engine:
    //            CarStatus.instance.engineHealth = healing;
    //            CarStatus.instance.engineQuality = quality;
    //            break;
    //        case UpgradeParts.Brakes:
    //            CarStatus.instance.brakesHealth = healing;
    //            CarStatus.instance.brakesQuality = quality;
    //            break;
    //        case UpgradeParts.Tires:
    //            CarStatus.instance.tiresHealth = healing;
    //            CarStatus.instance.tiresQuality = quality;
    //            break;
    //    }
    //    GarageManager.instance.UpUI.UpdateCurrentUpgradePanel(quality);
        
    //    //WE WOULD THEN WANT TO SET THE UI TO REFLECT THIS PURCHASE/REPAIR
    //}
}


