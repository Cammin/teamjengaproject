﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusUI : Singleton<StatusUI>
{
    private CarStatus stats;
    private CarCollision carCollision;
    private DestinationArrow destination;

    //These won't be displayed in the final game
    public Text bodyText;
    public Text engineText;
    public Text brakesText;
    public Text tiresText;
    public Text bodyFactorText;
    public Text engineFactorText;
    public Text brakesFactorText;
    public Text tiresForwardFactorText;
    public Text tiresSidewaysFactorText;
    public Text engineTorqueText;
    public Text brakesTorqueText;
    public Text tiresForwardFrictionText;
    public Text tiresSidewaysFrictionText;
    public Text tiresDeltaRotationText;
    public Text magnitudeText;
    public Text maxMagnitudeText;
    public Text destinationDistanceText;

    //All of this is what is displayed in the real game
    public Text moneyText;
    public Text rewardText;
    public Text timeRemaining;
    public Text endDayText;


    public List<GameObject> TextToDestroyWithoutDebug;

    // Use this for initialization
    void Start ()
    {
        if (CarStatus.instance != null)
        {
            stats = CarStatus.instance;
        }
        else
        {
            Debug.LogError("CarStatus not found!");
        }

        if (FindObjectOfType<CarCollision>() != null)
        {
            carCollision = FindObjectOfType<CarCollision>().GetComponent<CarCollision>();
        }
        else
        {
            Debug.LogError("CarCollision script not found!");
        }

        if (FindObjectOfType<DestinationArrow>() != null)
        {
            destination = FindObjectOfType<DestinationArrow>();
        }
        else
        {
            Debug.LogError("DestinationArrow script not found!");
        }

        //Deactivates all text gameobjects that are only meant for debugging/testing purposes
        if (GameManager.instance.hack.extraGameplayText == false)
        {
            foreach (GameObject text in TextToDestroyWithoutDebug)
            {
                text.SetActive(false);
            }
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        bodyText.text = "Body Durability: " + (stats.bodyHealth*100).ToString("F2") + "%";
        engineText.text = "Engine Durability: " + (stats.engineHealth * 100).ToString("F2") + "%";
        brakesText.text = "Brakes Durability: " + (stats.brakesHealth * 100).ToString("F2") + "%";
        tiresText.text = "Tires Durability: " + (stats.tiresHealth * 100).ToString("F2") + "%";

        bodyFactorText.text = "Body Factor: " + (stats.bodyFactor * 100).ToString("F2") + "%";
        engineFactorText.text = "Engine Performance: " + (stats.engineFactor * 100).ToString("F2") + "%";
        brakesFactorText.text = "Brakes Performance: " + (stats.brakesFactor * 100).ToString("F2") + "%";
        tiresForwardFactorText.text = "Tires Forwards Performance: " + (stats.tiresForwardsFactor * 100).ToString("F2") + "%";
        tiresSidewaysFactorText.text = "Tires Sideways Performance: " + (stats.tiresSidewaysFactor * 100).ToString("F2") + "%";

        engineTorqueText.text = "Engine Torque: " + (stats.currentMotorTorque).ToString("F2");
        brakesTorqueText.text = "Brakes Torque: " + (stats.currentBrakeTorque).ToString("F2");
        tiresForwardFrictionText.text = "Tires Forwards Friction: " + (stats.currentTiresForwardsFriction).ToString("F2");
        tiresSidewaysFrictionText.text = "Tires Sideways Friction: " + (stats.currentTiresSidewaysFriction).ToString("F2");
        tiresDeltaRotationText.text = "Tires Average RPM: " + (stats.tiresAverageRPM).ToString("F0") + "";

        moneyText.text = "Money: $" + GameManager.instance.money.ToString("F2");


        if (QuestScript.instance.deliveryState == DeliveryState.delivering)
        {
            rewardText.text = "Deliver the Pizza! Current Reward: $" + QuestScript.instance.currentReward.ToString("F2");
        }
        else
        {
            rewardText.text = "Retrieve a new order back at the Pizza place!";
        }


        UpdateTimerText(GameManager.instance.timeTemp);

        magnitudeText.text = "Speed: " + carCollision.currentMagnitude.ToString("F2") + " m/s";
        maxMagnitudeText.text = "Max tolerance: " + stats.impactTolerance.ToString("F2") + " m/s";

        destinationDistanceText.text = "Destination Distance: " + destination.distance.ToString("F2") + "m";
    }

    private void UpdateTimerText(float time)
    {
        if (time > 0.0f)
        {
            int minutes = Mathf.FloorToInt(time / 60);
            int seconds = Mathf.CeilToInt(time % 60);

            if (minutes > 0)
            {
                timeRemaining.text = "Time left: " + minutes.ToString() + "m "
                                              + seconds.ToString() + "s ";
            }
            else
            {
                timeRemaining.text = "Time left: " + seconds.ToString() + "s ";
            }
        }
        else
        {
            timeRemaining.text = "Time left: 0s";
        }
    }


}
