﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialScript : MonoBehaviour
{
    public Text onscreenText;
    public int textIndex = 0;
    public GameObject finalPizzaSpawnPoint;
    public GameObject destinationPrefab;
    Renderer arrowRender;

    private string[] tutorialString =
    {
        "Welcome to Pizza Please! In this game, you are a pizza delivery guy.\nPress Enter to advance.",
        "Use W, forwards arrow key, or up on the control stick to drive forward.\nDrive to the pizza pickup point.",
        "Good job. Once you've picked up the pizza, then you must deliver it.\nThe arrow near your car will point to your desination.",
        "Awesome! Your work isn't done yet; drive back to the pizza place and pick up another one.\nSteer with A & D, Left Arrow & Right Arrow, or left  & right on the control stick.",
        "Now deliver the final pizza to it's destination.\nYou can also use space to brake.",
        "You've completed the tutorial! In the normal game you may upgrade your car with the money you've earnt at your garage.\nPress Enter to finish the tutorial."
    };

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            LeaveTutorial();
        }
    }
    // Use this for initialization
    void Start ()
    {
        arrowRender = FindObjectOfType<DestinationArrow>().GetComponentInChildren<Renderer>(); 
        try
        {
            SetText();
        }
        catch
        {
            Debug.Log("No text object equipped!");
        }
    }

    public void FinishedTask()
    {
        if (textIndex >= 5)
        {
            LeaveTutorial();
            return;
        }

        ++textIndex;
        SetText(); 
    }

    void SetText()
    {
        onscreenText.text = tutorialString[textIndex];
        StartNewStep();
    }

    private void StartNewStep()
    {
        switch(textIndex)
        {
            case 0: // first showing text
                arrowRender.enabled = false;
                StartCoroutine(WaitForInput());
                Time.timeScale = 0;
                break;
            case 1: // telling player to drive to pickup
                GameManager.instance.startedTutorial = true;
                StartCoroutine(WaitForCollision());
                Time.timeScale = 1;
                break;
            case 2:// telling player to drive to destination
                StartCoroutine(WaitForCollision());
                Instantiate(destinationPrefab, finalPizzaSpawnPoint.transform);
                arrowRender.enabled = true;
                break;
            case 3://telling player to drive back to pickup
                StartCoroutine(WaitForCollision());
                break;
            case 4://telling player to 
                StartCoroutine(WaitForCollision());
                break;
            case 5://done, delivered final pizza
                StartCoroutine(WaitForInput());
                CarPizzaPlace place = FindObjectOfType<CarPizzaPlace>();
                Destroy(place.gameObject);
                arrowRender.enabled = false;
                GameManager.instance.finishedTutorial = true;
                break;
            default:
                Debug.Log("went past the tutorial index!");
                break;
        }
    }

    public bool tutorialPing = false;

    IEnumerator WaitForInput()
    {
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        FinishedTask();
    }

    IEnumerator WaitForCollision()
    {
        Debug.Log("waiting for collision");
        yield return new WaitUntil(() => tutorialPing == true);
        tutorialPing = false;
        FinishedTask();
    }

    void LeaveTutorial()
    {
        GameManager.instance.startedTutorial = false;
        QuestScript.instance.deliveryState = DeliveryState.none;
        SceneManagement.instance.ChangeScene(SceneManagement.instance.titleScreenSceneName);
        Time.timeScale = 1;
    }
}
