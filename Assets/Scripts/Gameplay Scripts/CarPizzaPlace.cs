﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarPizzaPlace : MonoBehaviour
{
    [HideInInspector]
    public bool isDestination = false;
    public float spinSpeed = 5;

    private void Update()
    {
        transform.eulerAngles += new Vector3(0, spinSpeed * Time.deltaTime, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<SimpleCarController>() == true && isDestination == true)
        {
            isDestination = false;
            QuestScript.instance.deliveryState = DeliveryState.delivering;
            QuestScript.instance.SetMission();

            if (GameManager.instance.startedTutorial)
            {
                TutorialScript tut = FindObjectOfType<TutorialScript>();
                tut.tutorialPing = true;
                
            }

            AudioBank.instance.PlaySound(snd.pizzaPickup);
        }
    }
}
