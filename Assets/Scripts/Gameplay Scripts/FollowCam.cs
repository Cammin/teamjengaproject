﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam : MonoBehaviour
{
    public Transform followTarget;
    public Vector3 cameraOffset;
    public float cameraFollowSpeed;

    public bool translatingEnabled = true;
    public bool rotatingEnabled = true;
    public bool xRotateIgnored = false;
    public bool yRotateIgnored = false;
    public bool zRotateIgnored = false;

    void Start ()
    {
        transform.position = followTarget.position + cameraOffset;
        transform.LookAt(followTarget, Vector3.up);
    }

    void FixedUpdate ()
    {
        if (translatingEnabled)
        {
            Vector3 currentPosition = transform.position;
            Vector3 targetPosition = followTarget.position + cameraOffset;

            Vector3 lerpPosition = Vector3.Lerp(currentPosition, targetPosition, cameraFollowSpeed);
            transform.position = lerpPosition;
        }
        else
        {
            transform.position = followTarget.position + cameraOffset;
        }
        
        if (rotatingEnabled)
        {
            Vector3 currentAngle = transform.eulerAngles;
            Vector3 targetAngle = followTarget.eulerAngles;

            float targetX;
            float targetY;
            float targetZ;

            if (xRotateIgnored)
            {
                targetX = currentAngle.x;
            }
            else
            {
                targetX = targetAngle.x;
            }

            if (yRotateIgnored)
            {
                targetY = currentAngle.y;
            }
            else
            {
                targetY = targetAngle.y;
            }

            if (zRotateIgnored)
            {
                targetZ = currentAngle.z;
            }
            else
            {
                targetZ = targetAngle.z;
            }

            transform.eulerAngles = new Vector3(Mathf.LerpAngle(currentAngle.x, targetX, cameraFollowSpeed),
                                                Mathf.LerpAngle(currentAngle.y, targetY, cameraFollowSpeed),
                                                Mathf.LerpAngle(currentAngle.z, targetZ, cameraFollowSpeed));
        }
        else
        {
            transform.LookAt(followTarget, Vector3.up);
        }
    }
}
