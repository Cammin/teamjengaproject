﻿public enum PartQuality
{
    Rusty,
    Standard,
    Heavy,
    Sporty,
    Royal
}