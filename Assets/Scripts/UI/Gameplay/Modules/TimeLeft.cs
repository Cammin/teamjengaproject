﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeLeft : MonoBehaviour {

    [SerializeField] Text CountdownText;

    public void UpdateTimer(int time) {
        int minutes = time / 60;
        int seconds = time - minutes * 60;
        CountdownText.text = minutes + ":" + ((seconds < 10) ? "0" + seconds : seconds.ToString());
    }
}
