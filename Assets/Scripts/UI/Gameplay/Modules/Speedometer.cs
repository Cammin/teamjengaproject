﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Speedometer : MonoBehaviour {

    [SerializeField] private float DisplayMultiplier;
    [SerializeField] private GameObject SpeedHighlighter;
    [SerializeField] private Text SpeedDisplay;
    [SerializeField] private Text SpeedDisplayShadow;

    public void SetSpeed(float speed) {
        SpeedHighlighter.transform.localScale = new Vector3(Mathf.Lerp(SpeedHighlighter.transform.localScale.x, Mathf.Clamp(speed / 100, 0, 1), 0.5f), 1, 1);
        SpeedDisplay.text = ((int) speed).ToString();
        SpeedDisplayShadow.text = ((int) speed).ToString();
    }

    public float GetDisplayMultiplier() {
        return DisplayMultiplier;
    }
}
