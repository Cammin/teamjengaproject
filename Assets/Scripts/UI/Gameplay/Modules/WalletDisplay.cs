﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WalletDisplay : MonoBehaviour {

    [SerializeField] Text walletValue;

    public void UpdateWalletValue(float money) {
        walletValue.text = money.ToString("C");
    }
}
