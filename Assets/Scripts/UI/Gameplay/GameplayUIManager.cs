﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplayUIManager : Singleton<GameplayUIManager> {

    public CarCollision CarCol { get; private set; }

    public TimeLeft Timeleft { get; private set; }
    public Speedometer Speedometer { get; private set; }
    public WalletDisplay Wallet { get; private set; }

    void Start () {
        CarCol = FindObjectOfType<CarCollision>().GetComponent<CarCollision>();

        Timeleft = GetComponentInChildren<TimeLeft>();
        Speedometer = GetComponentInChildren<Speedometer>();
        Wallet = GetComponentInChildren<WalletDisplay>();
    }

    private float kph = 0;

    private void LateUpdate() {
        float rawMps = CarCol != null ? CarCol.currentSpeed : 0f;
        float correctedMps = rawMps * Speedometer.GetDisplayMultiplier();
        float rawKph = rawMps * 3.6f;

        kph = Mathf.Round(rawKph);

        Speedometer.SetSpeed(kph);
        Timeleft.UpdateTimer((int) GameManager.instance.timeTemp);
        Wallet.UpdateWalletValue(GameManager.instance.money);
    }
}
