﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PopupMenu : MonoBehaviour {

    [SerializeField] private Button YesButton;
    [SerializeField] private Text YesText;

    [SerializeField] private Button NoButton;
    [SerializeField] private Text NoText;

    [SerializeField] private Button OkButton;
    [SerializeField] private Text OkText;

    [SerializeField] private Text messageText;

    private void Start()
    {
        
    }

    public void Setup(string message, string buttonText, UnityAction okAct)
    {
        OkButton.gameObject.SetActive(true);
        YesButton.gameObject.SetActive(false);
        NoButton.gameObject.SetActive(false);
        OkText.text = buttonText;
        messageText.text = message;
        OkButton.onClick.AddListener(okAct + delegate
        {
            Dispose();
        });
    }

    public void Setup(string message, string yesButton, string noButton, UnityAction yesAct, UnityAction noAct)
    {
        OkButton.gameObject.SetActive(false);
        YesButton.gameObject.SetActive(true);
        NoButton.gameObject.SetActive(true);
        NoText.text = noButton;
        YesText.text = yesButton;
        messageText.text = message;
        YesButton.onClick.AddListener(yesAct + delegate
        {
            Dispose();
        });
        NoButton.onClick.AddListener(noAct + delegate
        {
            Dispose();
        });
    }
    
    public void Dispose()
    {
        YesButton.onClick.RemoveAllListeners();
        NoButton.onClick.RemoveAllListeners();
        OkButton.onClick.RemoveAllListeners();
        Destroy(gameObject);
    }
}
