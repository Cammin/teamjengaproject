﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    [SerializeField] private float speed = 0.1f;

    [SerializeField] private float start = 12;
    [SerializeField] private float end = -25;

    private void Start() {
        transform.position = new Vector3(start, transform.position.y, transform.position.z);
    }

    private void FixedUpdate () {
        transform.position = new Vector3(transform.position.x + ((end < start) ? speed : -speed), transform.position.y, transform.position.z);
	}
}
