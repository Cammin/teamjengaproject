﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TitleScreenManager : Singleton<TitleScreenManager> {

    [TextArea(0, 20)] [SerializeField] string creditsString;

    [SerializeField] Button ButtonPlay;
    [SerializeField] Button ButtonTutorial;
    [SerializeField] Button ButtonCredit;
    [SerializeField] Button ButtonQuit;

    [SerializeField] GameObject PopupPrefab;

    private void Start() {
        ButtonPlay.onClick.AddListener(PlayBtn);
        ButtonTutorial.onClick.AddListener(TutorialBtn);
        ButtonCredit.onClick.AddListener(CreditBtn);
        ButtonQuit.onClick.AddListener(QuitBtn);
    }

    private void PlayBtn() {
        PlayButtonSound();

        if (GameManager.instance.finishedTutorial) {
            SceneManagement.instance.ChangeScene(SceneManagement.instance.garageSceneName);
        } else {
            OpenConfirmationDialog("It looks like you haven't played the tutorial yet.\nPlay the tutorial?", "Yes, play tutorial", "No, skip tutorial",
            () => // yes play tutorial
            {
                SceneManagement.instance.ChangeScene(SceneManagement.instance.tutorialSceneName);
            },
            () => // no skip tutorial and dont show popup again
            {
                SceneManagement.instance.ChangeScene(SceneManagement.instance.garageSceneName);
                GameManager.instance.finishedTutorial = true;
            });
        }
    }

    private void TutorialBtn() {
        PlayButtonSound();
        SceneManagement.instance.ChangeScene(SceneManagement.instance.tutorialSceneName);
    }

    private void CreditBtn() {
        PlayButtonSound();
        OpenMessageDialog(creditsString, "Thanks", () => ClosePopup());
    }

    private void QuitBtn() {
        PlayButtonSound();
        OpenConfirmationDialog("Are you sure you want to quit?", "Yes", "No", () => Application.Quit(), () => ClosePopup());
    }

    public void ClosePopup() {
        PlayButtonSound();

        ButtonPlay.GetComponent<Button>().enabled = true;
        ButtonTutorial.GetComponent<Button>().enabled = true;
        ButtonCredit.GetComponent<Button>().enabled = true;
        ButtonQuit.GetComponent<Button>().enabled = true;
        GetComponentInChildren<PopupMenu>().Dispose();
    }

    public void OpenMessageDialog(string message, string buttonText, UnityAction action) {
        ButtonPlay.GetComponent<Button>().enabled = false;
        ButtonTutorial.GetComponent<Button>().enabled = false;
        ButtonCredit.GetComponent<Button>().enabled = false;
        ButtonQuit.GetComponent<Button>().enabled = false;
        GameObject menu = Instantiate(PopupPrefab.gameObject, gameObject.transform);
        menu.GetComponent<PopupMenu>().Setup(message, buttonText, action);
    }

    public void OpenConfirmationDialog(string message, string yesButton, string noButton, UnityAction yesAction, UnityAction noAction) {

        ButtonPlay.GetComponent<Button>().enabled = false;
        ButtonTutorial.GetComponent<Button>().enabled = false;
        ButtonCredit.GetComponent<Button>().enabled = false;
        ButtonQuit.GetComponent<Button>().enabled = false;
        GameObject menu = Instantiate(PopupPrefab.gameObject, gameObject.transform);
        menu.GetComponent<PopupMenu>().Setup(message, yesButton, noButton, yesAction, noAction);
    }

    void PlayButtonSound() {
        AudioBank.instance.PlaySound(snd.button);
    }
}
