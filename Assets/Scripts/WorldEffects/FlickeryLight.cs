﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickeryLight : MonoBehaviour {

    [SerializeField]
    [Tooltip("The base interval of every chance of flickering (seconds)")]
    private float FlickerInterval = 0.25f;
    [SerializeField]
    [Tooltip("The chance of a light flickering every interval.")]
    [Range(0.05f, 1.0f)]
    private float FlickerRate = 0.05f;
    [SerializeField]
    [Tooltip("The intensity of the flickering, how much it effects the lights base intensity. (0.0 being fully off, 1.0 being basically uneffected)")]
    [Range(0.0f, 1.0f)]
    private float FlickerIntensity = 0.0f;

    private float timer = 0;
    private bool lightOff = false;
    private float previousIntensity = 1.0f;

    void Start () {}
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            if (!lightOff)
            {
                if (Random.value < FlickerRate)
                {
                    previousIntensity = gameObject.GetComponent<Light>().intensity;
                    gameObject.GetComponent<Light>().intensity = previousIntensity * FlickerIntensity;
                    lightOff = true;
                }
            } else
            {
                gameObject.GetComponent<Light>().intensity = previousIntensity;
                lightOff = false;
            }
            timer = FlickerInterval;
        }
    }
}
