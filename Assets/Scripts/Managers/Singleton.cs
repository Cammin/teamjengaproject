﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{

    protected static T _instance;

    //Gets set once called from outside script
    public static T instance
    {
        get
        {
            //Sets _instance if _instance is not already set
            if (_instance == null)
            {
                //Find object of type of generic TYPE inherited from monobehavior
                _instance = FindObjectOfType<T>();
            }
            return _instance;
        }
    }

    //Try to avoid overriding unity methods (blue functions) as unity finds them by reflection
    void Awake()
    {
        //Instead, overside a function inside of awake
        OnAwake();
    }

    protected virtual void OnAwake()
    {

        //Destroy singleton gameobject copy if there is more than 1
        if (instance != this)
        {
            Destroy(gameObject);
        }
    }




}