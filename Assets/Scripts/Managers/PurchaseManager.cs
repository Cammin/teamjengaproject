﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseManager : Singleton<PurchaseManager>
{
    Dictionary<Type, BaseCarPart> carParts = new Dictionary<Type, BaseCarPart>();

    public BaseCarPart currentPart;
    public string currentTitle;
    public string newDiscr;

    private void Awake()
    {
        ScreenInit();
    }
    void ScreenInit()
    {
        foreach (BaseCarPart part in FindObjectsOfType<BaseCarPart>())
        {
            //hide all screens on load
           // part.gameObject.SetActive(false);
            //Add to dictionary by key (generic type in this case) and by screen class (1 value per dictionary)
            carParts.Add(part.GetType(), part);
        }
    }
    //Manages what's being shown from a generic type of UIBasescreen
    public void Show<T>() where T : BaseCarPart
    {
        //Does not exist in our dictionary, print out statement
        if (carParts.ContainsKey(typeof(T)) == false)
        {
            // Debug.Log("UIBaseScreen of this type does not exist in dictionary.");
            //Wont allow us to continue the rest of our code in this function
            return;
        }
        //Set our current screen to the generic type of UIBaseScreen being passed
        currentPart = carParts[typeof(T)];

        GarageManager.instance.GarUI.ShowGeneralMenu(false);
        GarageManager.instance.GarUI.ShowPurchasingMenu(true);
        currentPart.ViewPartQualities();
        //Open :)
    }

    public void PurchasePart()
    {
        currentPart.equippedQuality = currentPart.partQuality;

        CarStatus.instance.LoadModel();
      //  GarageManager.instance.UpUI.UpdatePurchaseTemplate(currentTitle, currentPart.e);
    }

}
