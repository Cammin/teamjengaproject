﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : Singleton<SceneManagement>
{
    public string titleScreenSceneName;
    public string garageSceneName;
    public string gameplaySceneName;
    public string tutorialSceneName;


    [HideInInspector]
    public string currentScene;

	void Awake ()
    {
        currentScene = SceneManager.GetActiveScene().name;
	}

    private void Start()
    {
        SetMusic();
    }

    void Update ()
    {

	    if(Input.GetKey(KeyCode.Backslash) && GameManager.instance.hack.sceneMovementDebug == true)
        {
            if (currentScene == garageSceneName)
            {
                CarStatus.instance.bodyHealth = 2;
                CarStatus.instance.engineHealth = 2;
                CarStatus.instance.brakesHealth = 2;
                CarStatus.instance.tiresHealth = 2;
                gameplaySceneName = "Lvl1";
                ChangeScene(gameplaySceneName);
            }
            else
            {
                ChangeScene(garageSceneName);
            }
        }

        if (Input.GetKey(KeyCode.C) && GameManager.instance.hack.sceneMovementDebug == true)
        {
            if (currentScene == garageSceneName)
            {
                CarStatus.instance.bodyHealth = 2;
                CarStatus.instance.engineHealth = 2;
                CarStatus.instance.brakesHealth = 2;
                CarStatus.instance.tiresHealth = 2;
                gameplaySceneName = "ClintonCitySceneBackup1";
                ChangeScene(gameplaySceneName);
            }
            else
            {
                ChangeScene(garageSceneName);
            }
        }
    }

    //In the future, we can make this a smooth transition or something
    public void ChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
        currentScene = scene;
        SetMusic();
    }

    public bool CheckScene(string name)
    {
        Scene gameScene = SceneManager.GetSceneByName(name);
        bool loaded = gameScene.isLoaded;

        return loaded;
    }

    void SetMusic()
    {
        bgm music = 0;

        if (currentScene == titleScreenSceneName)
        {
            music = bgm.titleScreen;
        }
        else if (currentScene == garageSceneName)
        {
            music = bgm.garage;
        }
        else if (currentScene == gameplaySceneName)
        {
            music = bgm.gameplay;
        }
        else if (currentScene == tutorialSceneName)
        {
            music = bgm.tutorial;
        }
        else
        {
            return;
        }

        GameManager.instance.GetComponent<AudioBank>().PlayMusic(music);
    }
}
