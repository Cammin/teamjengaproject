﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Tier
{
    low,
    mid,
    high
}

[System.Serializable]
public class DebugHacks
{
    public bool sceneMovementDebug = true;

    public bool extraGameplayText = true;

    public bool bodyInvincibility = true;
    public bool engineInvincibility = true;
    public bool brakesInvincibility = true;
    public bool tiresInvincibility = true;
}

public class GameManager : Singleton<GameManager>
{
    public enum Apartment
    {
        shed,
        garage,
        mansion
    }

    //All GetSet References ~~~~
    public StatusUI UI { get; private set; }
    //All Alterable values ~~~~

    public DebugHacks hack;


    public Tier currentGarageTier;
    public float daysLived;
    [HideInInspector]
    public float timeTemp;
    public float time;
    public float money;

    public bool startedTutorial;
    public bool finishedTutorial;


    //All Private Values ~~~~
    private bool dayEnded;
    private bool dayStarted;


    // Use this for initialization
    void Awake ()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        timeTemp = time;
    }

    // Update is called once per frame
    void Update ()
    {
        if (SceneManagement.instance.currentScene == SceneManagement.instance.gameplaySceneName)
        {
            CarStatus.instance.CalculatePerformance();
            DayUpdate();
        }



    }

    public void StartDay()
    {
        if (dayStarted == false && SceneManagement.instance.CheckScene("GameScene"))
        {
            dayStarted = true;
            ObstacleManager.instance.StartDay();
          //  StatusUI.instance.endDayText.text = "";
        }
        

            



    }

    public void EndDay()
    {
        dayEnded = false;
        timeTemp = time;
        GetComponent<SceneManagement>().ChangeScene("Garage");
        QuestScript.instance.deliveryState = DeliveryState.none;
        Time.timeScale = 1;
        StatusUI.instance.endDayText.text = "";
        dayStarted = false;
        daysLived++;
    }

    void DayUpdate()
    {
        if (dayEnded == false)
        {
            if (CarStatus.instance.engineHealth <= 0)
            {
                dayEnded = true;
                StartCoroutine(CEndDay(2.0f));
                //DISPLAY TEXT SAYING THE DAY HAS ENDED BECAUSE THE ENGINE DIED
                StatusUI.instance.endDayText.text = "Engine no longer works! You're getting towed!";
            }

            if (timeTemp <= 0)
            {
                dayEnded = true;

                StartCoroutine(CEndDay(2.0f));
                //DISPLAY TEXT SAYING THE DAY HAS ENDED
                StatusUI.instance.endDayText.text = "The day has ended. Time to head back to the garage!";
            }
            else
            {
                timeTemp -= Time.deltaTime;
            }
        }

    }

    public void UpdateMoney(float price)
    {
        float newMoney = money - price;
        money = newMoney;
        GarageManager.instance.GarUI.UpdateMoneyUI();
        Debug.Log(money);
    }

    private IEnumerator CEndDay(float time)
    {
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(time);
        EndDay();
    }

}






