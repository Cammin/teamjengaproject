﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarageManager : Singleton<GarageManager>
{
    public GarageUI GarUI { get; private set; }
    public UpgradesUI UpUI { get; private set; }

    [SerializeField]
    FollowCam cameraObject;
    [SerializeField]
    GameObject defaultView;
    [SerializeField]
    GameObject bodyView;
    [SerializeField]
    GameObject engineView;
    [SerializeField]
    GameObject brakesView;
    [SerializeField]
    GameObject tiresView;
    
    // Use this for initialization
    void Start ()
    {
        GarUI = FindObjectOfType<GarageUI>();
        UpUI = FindObjectOfType<UpgradesUI>();
        cameraObject = FindObjectOfType<FollowCam>();
	}

    #region views
    public void EngineView()
    {
        cameraObject.followTarget = engineView.transform;
    }

    public void DefaultView()
    {
        cameraObject.followTarget = defaultView.transform;
    }

    public void BodyView()
    {
        cameraObject.followTarget = bodyView.transform;
    }

    public void BrakesView()
    {
        cameraObject.followTarget = brakesView.transform;
    }
    public void TiresView()
    {
        cameraObject.followTarget = tiresView.transform;
    }
    #endregion
}
