﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    [SerializeField] Canvas ConfirmationWindow;
    [SerializeField] Canvas WarningWindow;

    [SerializeField] string ConfirmationWindowDefaultYesButtonText;
    [SerializeField] string ConfirmationWindowDefaultNoButtonText;
    [SerializeField] string ErrorWindowDefaultButtonText;

    [SerializeField] Sprite ConfirmWindowDefaultSprite;
    [SerializeField] Sprite ErrorWindowDefaultSprite;

    public static void OpenConfirmationWindow(string message)
    {

    }

    public static void OpenConfirmationWindow(string message, string noButtonText, string yesButtonTexts)
    {

    }

    public static void OpenConfirmationWindow(string message, string noButtonText, string yesButtonTexts, Sprite Icon)
    {

    }

    public static void OpenErrorWindow(string message)
    {

    }

    public static void OpenErrorWindow(string message, string buttonText)
    {

    }

    public static void OpenErrorWindow(string message, string buttonText, Sprite Icon)
    {

    }
}
