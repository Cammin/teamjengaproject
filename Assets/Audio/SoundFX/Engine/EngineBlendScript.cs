﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineBlendScript : MonoBehaviour
{
    [HideInInspector] public float targetPoint;

    [Tooltip("This is the Animator that will be controlled")]
    public Animator animator;

    [Tooltip("This is the name of the state to control in the Animator")]
    public string animState;

    [Range(0, 1f)]
    public float animFrame;

    public float RPMAtMax = 3000;
    public float smoothSoundLerpCoefficient = 0.2f;

    void Update()
    {
        float RPM = Mathf.Abs(GameManager.instance.GetComponent<CarStatus>().tiresAverageRPM);
        targetPoint = Mathf.Clamp01(RPM / RPMAtMax);
        animFrame = Mathf.Lerp(animFrame, targetPoint, smoothSoundLerpCoefficient);

        animator.Play(animState, 0, animFrame);

        
    }
}
