﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum bgm
{
    titleScreen,
    garage,
    gameplay,
    tutorial,
}

public enum snd
{
    // TitleScreen
    button,

    // Garage
    upgradeBought,
    upgradeDenied,
    carHoodOpen,
    carHoodClose,
    wheelReplace,

    // Gameplay
    impact,
    engineIdle, 
    engineAccelerate,
    moneyCollected,
    pizzaPickup,
}

public class AudioBank : Singleton<AudioBank>
{
    public GameObject engineAudioObject;

    #region Music
    [Header("Music")]
    public AudioClip titleScreen;
    public AudioClip garage;
    public AudioClip gameplay;
    public AudioClip tutorial;
    #endregion
    #region Sounds
    [Header("Sounds")]
    public AudioClip button;
    public AudioClip upgradeBought;
    public AudioClip upgradeDenied;
    public AudioClip carHoodOpen;
    public AudioClip carHoodClose;
    public AudioClip wheelReplace;
    public AudioClip impact;
    public AudioClip engineIdle;
    public AudioClip engineAccelerate;
    public AudioClip moneyCollected;
    public AudioClip pizzaPickup;
    #endregion

    [HideInInspector] public AudioSource source;
    [HideInInspector] public AudioSource musicSource;

    public void PlayMusic(bgm audio)
    {
        switch (audio)
        {
            case bgm.titleScreen:
                musicSource.clip = titleScreen;
                break;
            case bgm.garage:
                musicSource.clip = garage;
                break;
            case bgm.gameplay:
                musicSource.clip = gameplay;
                break;
            case bgm.tutorial:
                musicSource.clip = tutorial;
                break;

            default:
                Debug.LogError("Failed to play music: " + audio.ToString());
                break;
        }
        musicSource.Play();
    }

    public void PlaySound(snd audio)
    {
        switch (audio)
        {
            case snd.button:
                source.PlayOneShot(button, 0.5f);
                break;
            case snd.upgradeBought:
                source.PlayOneShot(upgradeBought, 0.3f);
                break;
            case snd.upgradeDenied:
                source.PlayOneShot(upgradeDenied, 0.5f);
                break;
            case snd.carHoodOpen:
                source.PlayOneShot(carHoodOpen, 1.0f);
                break;
            case snd.carHoodClose:
                source.PlayOneShot(carHoodClose, 1.0f);
                break;
            case snd.wheelReplace:
                source.PlayOneShot(wheelReplace, 1.0f);
                break;
            case snd.engineIdle:
                source.PlayOneShot(engineIdle, 1.0f);
                break;
            case snd.engineAccelerate:
                source.PlayOneShot(engineAccelerate, 1.0f);
                break;
            case snd.moneyCollected:
                source.PlayOneShot(moneyCollected, 0.3f);
                break;
            case snd.pizzaPickup:
                source.PlayOneShot(pizzaPickup, 0.4f);
                break;

            default:
                Debug.LogError("Failed to play sound: " + audio.ToString());
                break;
        }
    }

    public void PlayImpact(float volume)
    {
        source.PlayOneShot(impact, Mathf.Clamp01(volume) * 0.2f);

    }

    private void Awake()
    {
        source = GetComponent<AudioSource>();
        musicSource = GetComponentInChildren<MusicPlayer>().GetComponent<AudioSource>();
    }

    private void Update()
    {
        
        if (engineAudioObject != null)
        {
            if (SceneManagement.instance.currentScene == SceneManagement.instance.gameplaySceneName
            ||  SceneManagement.instance.currentScene == SceneManagement.instance.tutorialSceneName)
            {
                engineAudioObject.gameObject.SetActive(true);
            }
            else
            {
                engineAudioObject.gameObject.SetActive(false);
            }
        }
        else
        {
            Debug.Log("Engine Audio component not found or set!");
        }

    }
}
